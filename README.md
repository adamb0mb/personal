How to configure your machine
=============================

This is a place for Adam to start automating the configuration of his machines.
It's mostly for getting stuff installed, but I'm open to other things too.

How to use
----------

* On a mac:
    ```bash
    cd mac
    chmod +x bootstrap.sh
    ./bootstrap.sh
    ```
